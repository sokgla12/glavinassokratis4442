package org.example;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;


import static org.junit.Assert.*;

public class MessageServiceTest {

    Network network = Mockito.mock(Network.class);


    @Test
    public void messageFailedToSend(){
        Mockito.when(network.sendMessage("192.168.1.15","Message was not send")).thenReturn(false);
        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.1.15","Message was not send");
        assertFalse(messageService.sendMessage("192.168.1.15","Message was not send"));
    }

    @Test
    public void messageWillBeSendSuccessfully(){
        Mockito.when(network.sendMessage("192.168.1.12","Message was send")).thenReturn(true);
        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.1.12","Message was send");
        assertTrue(messageService.sendMessage("192.168.1.12","Message was received"));
    }



}